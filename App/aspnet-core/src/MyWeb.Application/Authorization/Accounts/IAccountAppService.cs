﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MyWeb.Authorization.Accounts.Dto;

namespace MyWeb.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
