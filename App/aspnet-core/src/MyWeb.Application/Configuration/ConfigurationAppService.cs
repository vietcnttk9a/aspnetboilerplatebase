﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using MyWeb.Configuration.Dto;

namespace MyWeb.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : MyWebAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
