﻿using System.Threading.Tasks;
using MyWeb.Configuration.Dto;

namespace MyWeb.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
