﻿using Abp.Application.Services;
using MyWeb.MultiTenancy.Dto;

namespace MyWeb.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

