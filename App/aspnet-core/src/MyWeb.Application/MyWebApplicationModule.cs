﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MyWeb.Authorization;

namespace MyWeb
{
    [DependsOn(
        typeof(MyWebCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class MyWebApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<MyWebAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(MyWebApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
