﻿using Abp.Application.Services.Dto;

namespace MyWeb.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

