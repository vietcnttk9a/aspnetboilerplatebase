﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MyWeb.Sessions.Dto;

namespace MyWeb.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
