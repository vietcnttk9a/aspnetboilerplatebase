using System.ComponentModel.DataAnnotations;

namespace MyWeb.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}