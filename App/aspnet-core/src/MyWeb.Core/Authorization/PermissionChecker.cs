﻿using Abp.Authorization;
using MyWeb.Authorization.Roles;
using MyWeb.Authorization.Users;

namespace MyWeb.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
