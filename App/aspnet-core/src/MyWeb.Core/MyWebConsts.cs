﻿using MyWeb.Debugging;

namespace MyWeb
{
    public class MyWebConsts
    {
        public const string LocalizationSourceName = "MyWeb";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;


        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public static readonly string DefaultPassPhrase =
            DebugHelper.IsDebug ? "gsKxGZ012HLL3MI5" : "d367a4a373cb489c931ff61f116e7cbe";
    }
}
