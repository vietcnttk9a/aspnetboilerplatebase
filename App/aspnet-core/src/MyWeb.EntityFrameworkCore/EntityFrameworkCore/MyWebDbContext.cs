﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using MyWeb.Authorization.Roles;
using MyWeb.Authorization.Users;
using MyWeb.MultiTenancy;

namespace MyWeb.EntityFrameworkCore
{
    public class MyWebDbContext : AbpZeroDbContext<Tenant, Role, User, MyWebDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public MyWebDbContext(DbContextOptions<MyWebDbContext> options)
            : base(options)
        {
        }
    }
}
