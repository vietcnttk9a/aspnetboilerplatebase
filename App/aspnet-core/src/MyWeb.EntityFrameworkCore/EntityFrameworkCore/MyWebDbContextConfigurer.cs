using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace MyWeb.EntityFrameworkCore
{
    public static class MyWebDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<MyWebDbContext> builder, string connectionString)
        {
            //builder.UseSqlServer(connectionString);
            builder.UseMySql(connectionString, MySqlServerVersion.LatestSupportedServerVersion);
        }

        public static void Configure(DbContextOptionsBuilder<MyWebDbContext> builder, DbConnection connection)
        {
            //builder.UseSqlServer(connection);
            builder.UseMySql(connection, MySqlServerVersion.LatestSupportedServerVersion);
        }
    }
}
