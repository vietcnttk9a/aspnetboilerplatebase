﻿using System.Threading.Tasks;
using MyWeb.Models.TokenAuth;
using MyWeb.Web.Controllers;
using Shouldly;
using Xunit;

namespace MyWeb.Web.Tests.Controllers
{
    public class HomeController_Tests: MyWebWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}