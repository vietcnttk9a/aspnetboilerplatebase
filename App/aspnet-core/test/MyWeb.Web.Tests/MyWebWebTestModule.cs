﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MyWeb.EntityFrameworkCore;
using MyWeb.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace MyWeb.Web.Tests
{
    [DependsOn(
        typeof(MyWebWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class MyWebWebTestModule : AbpModule
    {
        public MyWebWebTestModule(MyWebEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MyWebWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(MyWebWebMvcModule).Assembly);
        }
    }
}